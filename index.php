<!DOCTYPE html>
<html>
  <head>
    <title>How to Use JSON Data with PHP or JavaScript</title>  
  </head>
  <body>
    <h3>How to Use JSON Data with PHP or JavaScript</h3>

    <p>First we will write some JSON data into a variable in PHP.</p>

    <?php
      $data = '{
        "name": "Aragorn",
        "race": "Human"
      }';

      echo $data;
    ?>

    <p>Then we'll use the json_decode function to convert the JSON string into a PHP object.</p>

    <?php
      $character = json_decode($data);
    ?>

    <p> Now we can access it as a PHP object.</p>
    
    Name: 
    <?php
      echo $character->name;
    ?>


  </body>
</html>
